/*
  Copyright (C) 2019 Federico Braghiroli
  Author: Federico Braghiroli <federico.braghiroli@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PIPING_PROTOCOL_H
#define PIPING_PROTOCOL_H

#include <stdio.h>

/* Frame structure:
 * 4 bytes -> magic
 * 2 bytes -> size
 * n bytes -> piping protocol data
 *
 * Piping Protocol data:
 * 1 byte -> pipe_id
 * * bit[7] return code request/response
 * * bit[6] pipe type (0: radio, 1: control)
 * * bit[5:0] pipe src (only when receiving data from radio)
 * 5 bytes -> rx/tx addr (only when sending data to radio)
 * n bytes -> radio/control/rc data
 *
 * Control data:
 * 1 byte -> ioctl id
 * n bytes -> ioctl data
 *
 * Radio data:
 * n bytes -> raw data
 *
 * Return code data:
 * 4 bytes: return code
 */

/* Frame */
#define FRAME_SIZE_MAX 64 /* tune this value for low memory systems */
#define FRAME_PP_OFFSET 6 /* magic + size */

/* Piping Protocol frame data */
#define PP_PIPE_ID_OFFSET 0
#define PP_ADDR_OFFSET 1
#define PP_DATA_OFFSET 6 /* pipe_id + rx/tx addr */
#define PP_PIPE_COUNT 6
#define PP_RCR_MASK 0x80
#define PP_PIPE_TYPE_MASK 0x40
#define PP_PIPE_SRC_MASK 0x3F
#define PP_RCR(x) (x & PP_RCR_MASK)
#define PP_PIPE_CTL(x) (x & PP_PIPE_TYPE_MASK)
#define PP_PIPE_RADIO(x) (!PP_PIPE_CTL(x))
#define PP_PIPE_SRC(x) (x & PP_PIPE_SRC_MASK)

/* Control data */
#define PP_CTLD_IOC_OFFSET (PP_DATA_OFFSET + 0)
#define PP_CTLD_DATA_OFFSET (PP_DATA_OFFSET + 1)

enum pp_frame_sts {
	PP_FS_IDLE,
	PP_FS_MAGIC,
	PP_FS_SIZE,
	PP_FS_DATA,
};

struct pp_data {
	enum pp_frame_sts fs;
	uint8_t magic_n;
	uint16_t f_size;
	uint16_t f_left;
	/* Frame buf is used to build the receving frame.
	 * It can't be used unless a frame has been completed.
	 * In a better world we should define a tx buffer
	 * to be used to build response frame.
	 */
	unsigned char frame_buf[FRAME_SIZE_MAX];
};

int pp_init(struct pp_data *d);
int pp_fetch(struct pp_data *d, const unsigned char *buf_in,
	     size_t len_in, int dev_fd, int sock);
int pp_encode(unsigned char *buf, size_t buf_len, int dev_fd);

#endif
