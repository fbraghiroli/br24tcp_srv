/*
  Copyright (C) 2019 Federico Braghiroli
  Author: Federico Braghiroli <federico.braghiroli@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdarg.h>
#include <fcntl.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <poll.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include "br24tcp.h"

#ifndef LINUX_BUILD
#include <sys/boardctl.h>
#include "nrf24_ctl.h"
#endif

#define eprintf(...) fprintf(stderr, __VA_ARGS__)
#define dprintf(...) fprintf(stderr, __VA_ARGS__)

#ifdef LINUX_BUILD
#define DEV_PATH "/tmp/test_fifo"
#else
#define DEV_PATH "/dev/nrf24l01"
#endif

#define SRV_PORT 9999
#define POLL_TOUT 500
/* tcp clients + 1 dev fd + 2 listen socket */
#define MAX_POLLFDS (MAX_CLIENTS + 2)

struct br24tcp_data {
	struct srv_data srv;
	struct pollfd pfds[MAX_POLLFDS];
	struct dev_data dev;
	int nfds;
};

struct br24tcp_data priv;

#ifdef LINUX_BUILD
int (*dev_cfg)(int fd) = NULL;
#else
int (*dev_cfg)(int fd) = nrf24_cfg;
#endif

#if defined(LINUX_BUILD)
int main(int argc, char *argv[])
#else
int br24tcp_main(int argc, char *argv[])
#endif
{
	char *dev;
	int ret = 0;
	int flags;
	priv.nfds = 0;

#ifndef NSH_AVAILABLE
#ifdef CONFIG_NSH_ARCHINIT
	/* Perform architecture-specific initialization (if configured) */
	(void)boardctl(BOARDIOC_INIT, 0);
#endif
	/* Bring up the network */
	(void)nsh_netinit();
#endif

	if (argc == 2) {
		printf("Using %s\n", argv[1]);
		dev = argv[1];
	} else {
		dev = DEV_PATH;
	}

	if ((priv.dev.fd = open(dev, O_RDWR)) < 0) {
		eprintf("Failed to open %s: %d\n", dev, -errno);
		ret = -errno;
		goto exit_err;
	}

	if (dev_cfg)
		dev_cfg(priv.dev.fd);

	flags = fcntl(priv.dev.fd, F_GETFL, 0);
	fcntl(priv.dev.fd, F_SETFL, flags | O_NONBLOCK);

	priv.pfds[priv.nfds].fd = priv.dev.fd;
	priv.pfds[priv.nfds].events = POLLIN;
	priv.nfds++;

	if ((ret = init_socks(&priv.srv, SRV_PORT)) < 0)
		goto exit_err_with_dev;

	/* try to specify maximum of 3 pending connections for the
	   master socket */
	if (listen(priv.srv.master_sock, 3) < 0) {
		eprintf("listen failed: %d", -errno );
		ret = -errno;
		goto exit_err_with_dev;
	}

	printf("Hello, World!!\n");
	priv.pfds[priv.nfds].fd = priv.srv.master_sock;
	priv.pfds[priv.nfds].events = POLLIN;
	priv.nfds++;

	while(1) {
		int evts;
		int i;
		evts = poll(priv.pfds, priv.nfds, POLL_TOUT);
		if (evts == 0) {
			//dprintf("poll timeout\n");
			continue;
		} else if (evts == -1) {
			dprintf("poll error: %d\n", -errno);
			continue;
		} else {
			dprintf("evts: %d\n", evts);
		}

		/* Check which fd is ready */
		for (i = 0; i < priv.nfds; i++) {
			if (priv.pfds[i].fd == priv.srv.master_sock) {
				int cfd = -1;
				process_master(&priv.srv, &priv.pfds[i], &cfd);
				if (cfd > 0) {
					priv.pfds[priv.nfds].fd = cfd;
					priv.pfds[priv.nfds].events = POLLIN;
					priv.nfds++;
				}
			} else if (priv.pfds[i].fd == priv.dev.fd) {
				process_dev(&priv.dev, &priv.pfds[i],
					   priv.srv.clis);
			} else {
				struct cli_data cp;
				/* Be careful: here we don't check the fd (sock)
				   to be in the list of connected client. If
				   something strange happens do it. */
				if (find_cli(&priv.srv, priv.pfds[i].fd, &cp)) {
					int ret_c;
					ret_c = process_client(&priv.srv, &cp,
							       &priv.pfds[i],
							       &priv.dev);
					if (ret_c == -ECONNRESET) {
						/* remove fd from polling list by
						   shifting the array.*/
						remove_pfd(priv.pfds, &priv.nfds, i);
						/* Ugly! current index refers to the
						   next element, so you need to
						   decrement it to avoid skipping an
						   element. */
						if (i > 0)
							i--;
						continue;
					}
				} else {
					dprintf("Sock %d not found\n", priv.pfds[i].fd);
				}
			}
		}
	}

	return 0;

exit_err_with_dev:
	close(priv.dev.fd);
	/* TODO: clean masters and clients sockets */
exit_err:
	printf("exit\n");
	return ret;
}
