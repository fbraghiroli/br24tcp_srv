/*
  Copyright (C) 2019 Federico Braghiroli
  Author: Federico Braghiroli <federico.braghiroli@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BR24TCP_H
#define BR24TCP_H

#include <poll.h>
#include "piping_protocol.h"

#define MAX_CLIENTS 1
#define SRV_BUFFIN_SIZE (64 + 20) /* 1 pp frame + some overhead */
#define DEV_BUFFIN_SIZE 64

struct cli_data {
	int sock;
	struct sockaddr_in addr;
	struct pp_data ppd;
};

struct srv_data {
	int master_sock;
	struct sockaddr_in addr;
	struct cli_data clis[MAX_CLIENTS];
	uint8_t buff_in[SRV_BUFFIN_SIZE];
};

struct dev_data {
	int fd;
	uint8_t buff_in[DEV_BUFFIN_SIZE];
};

int find_cli(const struct srv_data *d, int sock, struct cli_data *cli);
void remove_pfd(struct pollfd *pfds, int *nfds, int id);
int init_socks(struct srv_data *d, int port);
int process_master(struct srv_data *d, struct pollfd *pfd, int *cfd);
int process_client(struct srv_data *d, struct cli_data *c,
		   struct pollfd *pfd, const struct dev_data *dev);
int process_client_ctl(struct srv_data *d, struct pollfd *pfd,
		   const struct dev_data *dev);
int process_dev(struct dev_data *d, struct pollfd *pfd,
		const struct cli_data *clid);

#endif
