/*
  Copyright (C) 2019 Federico Braghiroli
  Author: Federico Braghiroli <federico.braghiroli@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdarg.h>
#include <fcntl.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <poll.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include "br24tcp.h"
#include "piping_protocol.h"

#define eprintf(...) fprintf(stderr, __VA_ARGS__)
#define dprintf(...) fprintf(stderr, __VA_ARGS__)

/* TODO:
 * It sucks!
 * Change this function to return, by argument, a pointer to the client
 * data; avoid to copy the entire struct!!
 */
int find_cli(const struct srv_data *d, int sock, struct cli_data *cli)
{
	int i;
	for (i = 0; i < MAX_CLIENTS; i++) {
		if (d->clis[i].sock == sock) {
			if (cli)
				*cli = d->clis[i];
			return 1;
		}
	}
	return 0;
}

void remove_pfd(struct pollfd *pfds, int *nfds, int id)
{
	for (; id < *nfds-1; id++)
		pfds[id] = pfds[id+1];

	memset(&pfds[id], 0, sizeof(struct pollfd));
	(*nfds)--;
}

int init_socks(struct srv_data *d, int port)
{
	uint16_t i;
	int opt;

	for (i = 0; i < MAX_CLIENTS; i++)
		d->clis[i].sock = 0;
	if ((d->master_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		eprintf("master socket failed: %d\n", -errno);
		return -errno;
	}

	/* bind socket to localhost with given port */
	d->addr.sin_family = AF_INET;
	d->addr.sin_addr.s_addr = INADDR_ANY;
	d->addr.sin_port = htons(port);
	if (bind(d->master_sock, (struct sockaddr *)&d->addr,
		 sizeof(d->addr)) < 0) {
		eprintf("bind failed: %d\n", -errno);
		return -errno;
	}

	/* set master socket to allow multiple connections,
	   this is just a good habit, it will work without this. */
	opt = 1;
	if (setsockopt(d->master_sock, SOL_SOCKET, SO_REUSEADDR, &opt,
		       sizeof(opt)) < 0) {
		eprintf("setsockopt failed: %d\n", -errno);
		return -errno;
	}

	return 0;
}

/*
 * Return values:
 * - negative errno in case of error
 * - 0 otherwise
 */
int process_master(struct srv_data *d, struct pollfd *pfd, int *cfd)
{
	if (pfd->revents & POLLIN) {
		socklen_t addrlen = sizeof(struct sockaddr_in);
		int cli_i;
		int flags;
		/* New connection! */
		dprintf("We got a new connection!\n");

		for (cli_i = 0; cli_i < MAX_CLIENTS; cli_i++)
			if (d->clis[cli_i].sock == 0)
				break;
		if (cli_i == MAX_CLIENTS) {
			eprintf("Max clients reached\n");
			/* TODO: master sock need to be cleared? */
			return -EUSERS;
		}

		d->clis[cli_i].sock = accept(d->master_sock, (struct sockaddr *)
					     &d->clis[cli_i].addr, &addrlen);
		if (d->clis[cli_i].sock < 0) {
			eprintf("accept failed: %d\n", -errno);
			d->clis[cli_i].sock = 0;
			return -errno;
		}
		flags = fcntl(d->clis[cli_i].sock, F_GETFL, 0);
		fcntl(d->clis[cli_i].sock, F_SETFL, flags | O_NONBLOCK);
		dprintf("Connection %d: fd: %d, ip: %s, port: %d\n", cli_i,
			d->clis[cli_i].sock,
			inet_ntoa(d->clis[cli_i].addr.sin_addr),
			ntohs(d->clis[cli_i].addr.sin_port));

		if (cfd)
			*cfd = d->clis[cli_i].sock;
#ifndef LINUX_BUILD
		/* pp_init for each new client accepted! */
		pp_init(&d->clis[cli_i].ppd);
#endif
	}
	return 0;
}

static int forward_dev(const struct dev_data *d, char *buf)
{
	int len;
	len = strlen(buf);
	write(d->fd, buf, len);
	return 0;
}

/* Return number of byte copied in the buffer,
 * negative number in case of error. */
int get_data(struct srv_data *d, struct pollfd *pfd)
{
	int ret = 0;
	int n_in = -1;

	if (pfd->revents & POLLIN)
		n_in = read(pfd->fd, d->buff_in, SRV_BUFFIN_SIZE);

	if (n_in == 0 || pfd->revents & (POLLERR|POLLNVAL)) {

		struct sockaddr_in addr;
		int addrlen = sizeof(addr);
		int cli_i;


		for (cli_i = 0; cli_i < MAX_CLIENTS; cli_i++) {
			if (d->clis[cli_i].sock == pfd->fd)
				break;
		}
		getpeername(pfd->fd, (struct sockaddr *)&addr,
			    (socklen_t *)&addrlen);
		dprintf("Disconnected %d: fd: %d ip: %s, port %d\n",
			cli_i, pfd->fd, inet_ntoa(addr.sin_addr),
			ntohs(addr.sin_port));
		close(pfd->fd);
		if (cli_i == MAX_CLIENTS) {
			dprintf("socket not found\n");
		} else {
			d->clis[cli_i].sock = 0;
		}
		ret = -ECONNRESET;
	} else {
		/* process the message */
		d->buff_in[n_in] = '\0';
		ret = n_in;
	}

	return ret;
}

int process_client(struct srv_data *d, struct cli_data *c,
		   struct pollfd *pfd, const struct dev_data *dev)
{
	int ret;
	if ((ret = get_data(d, pfd)) > 0) {
		/* process the message */
		pp_fetch(&(c->ppd), d->buff_in, ret, dev->fd, c->sock);
	}

	return ret;
}

int process_client_ctl(struct srv_data *d, struct pollfd *pfd,
		       const struct dev_data *dev)
{
	int ret;
	if ((ret = get_data(d, pfd)) > 0) {
		printf("ctl buff: %s\n", d->buff_in);
	}

	return ret;
}

static int forward_to_tcp(const struct cli_data *c, int n, char *buf, size_t l)
{
	int i;

	for (i = 0; i < n; i++)
		if (c[i].sock != 0)
			write(c[i].sock, buf, l);

	return 0;
}

int process_dev(struct dev_data *d, struct pollfd *pfd,
		const struct cli_data *clid)
{
	if (pfd->revents & POLLIN) {
		int ret;
#ifdef LINUX_BUILD
		size_t n_in;
		if ((n_in = read(pfd->fd, d->buff_in, DEV_BUFFIN_SIZE-1)) == 0) {
			printf("nothing to read !?!?!\n");
		} else {
			d->buff_in[n_in] = '\0';
			printf("Data: %s\n", d->buff_in);
			forward_to_tcp(clid, MAX_CLIENTS, (char *)d->buff_in,
				       strlen(d->buff_in));
		}
#else
		if ((ret = pp_encode(d->buff_in, DEV_BUFFIN_SIZE, pfd->fd)) < 0) {
			dprintf("pp_encode failed: %d\n", ret);
		} else if (ret) {
			forward_to_tcp(clid, MAX_CLIENTS, (char *)d->buff_in, ret);
		}
#endif
	}

	return 0;
}
