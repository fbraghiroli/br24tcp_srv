/****************************************************************************
 * examples/nrf24l01_term/nrf24l01_term.c
 *
 *   Copyright (C) 2013 Laurent Latil. All rights reserved.
 *   Author: Laurent Latil <laurent@latil.nom.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/wireless/nrf24l01.h>
#include <sys/ioctl.h>
#include <string.h>
#include "nrf24_ctl.h"

#define DEFAULT_RADIOFREQ  2450

#define DEFAULT_TXPOWER    -6    /* (0, -6, -12, or -18 dBm) */

static const uint8_t defaultaddr[NRF24L01_MAX_ADDR_LEN] = { 0x01, 0xCA, 0xFE, 0x12, 0x34};

int nrf24_cfg(int fd)
{
	int error = 0;

	uint32_t rf = DEFAULT_RADIOFREQ;
	int32_t txpow = DEFAULT_TXPOWER;
	nrf24l01_datarate_t datarate = RATE_1Mbps;

	nrf24l01_retrcfg_t retrcfg =
		{
			.count = 5,
			.delay = DELAY_1000us
		};

	uint32_t addrwidth = NRF24L01_MAX_ADDR_LEN;

	uint8_t pipes_en = (1 << 0);  /* Only pipe #0 is enabled */
	nrf24l01_pipecfg_t pipe0cfg;
	nrf24l01_pipecfg_t *pipes_cfg[NRF24L01_PIPE_COUNT] = {&pipe0cfg, 0, 0, 0, 0, 0};

	nrf24l01_state_t primrxstate;

	primrxstate = ST_RX;

	/* Define the pipe #0 parameters  (AA enabled, and dynamic payload length) */

	pipe0cfg.en_aa = true;
	pipe0cfg.payload_length = NRF24L01_DYN_LENGTH;
	memcpy (pipe0cfg.rx_addr, defaultaddr, NRF24L01_MAX_ADDR_LEN);

	/* Set radio parameters */

	ioctl(fd, WLIOC_SETRADIOFREQ, (unsigned long)((uint32_t *)&rf));
	ioctl(fd, WLIOC_SETTXPOWER, (unsigned long)((int32_t *)&txpow));
	ioctl(fd, NRF24L01IOC_SETDATARATE, (unsigned long)((nrf24l01_datarate_t *)&datarate));
	ioctl(fd, NRF24L01IOC_SETRETRCFG, (unsigned long)((nrf24l01_retrcfg_t *)&retrcfg));

	ioctl(fd, NRF24L01IOC_SETADDRWIDTH, (unsigned long)((uint32_t*) &addrwidth));
	ioctl(fd, NRF24L01IOC_SETTXADDR, (unsigned long)((uint8_t *)&defaultaddr));

	ioctl(fd, NRF24L01IOC_SETPIPESCFG, (unsigned long)((nrf24l01_pipecfg_t **)&pipes_cfg));
	ioctl(fd, NRF24L01IOC_SETPIPESENABLED, (unsigned long)((uint8_t *)&pipes_en));

	/* Enable receiver */

	ioctl(fd, NRF24L01IOC_SETSTATE, (unsigned long)((nrf24l01_state_t *)&primrxstate));

	return error;
}

