/*
  Copyright (C) 2019 Federico Braghiroli
  Author: Federico Braghiroli <federico.braghiroli@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* This protocol is highly tied to nrf24l01 device driver written for
 * NuttX rtos.
 * I have no time to make this generic. The only things I may
 * implement for LINUX_BUILD are just simple message forward. */

#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <nuttx/wireless/nrf24l01.h>
#include "piping_protocol.h"

#define eprintf(...) fprintf(stderr, __VA_ARGS__)
#define dprintf(...) fprintf(stderr, __VA_ARGS__)

#define MAGIC0 0x10
#define MAGIC1 0xbe
#define MAGIC2 0xef
#define MAGIC3 0x10
#define SIZE_N 2 /* size: 2 bytes, big endian;  64k should be enough */

const char MAGIC[] = {0x10, 0xbe, 0xef, 0x10};

struct ctl_el {
	uint16_t ioctl;
	uint8_t size_i;
	uint8_t size_o;
};

enum ctl_id {
	CTL_SETRADIOFREQ,
	CTL_GETRADIOFREQ,
	CTL_SETTXPOWER,
	CTL_GETTXPOWER,
	CTL_SETRETRCFG,
	CTL_GETRETRCFG,
	CTL_SETPIPESCFG,
	CTL_GETPIPESCFG,
	CTL_SETPIPESENABLED,
	CTL_GETPIPESENABLED,
	CTL_SETDATARATE,
	CTL_GETDATARATE,
	CTL_SETADDRWIDTH,
	CTL_GETADDRWIDTH,
	CTL_SETSTATE,
	CTL_GETSTATE,
	CTL_GETLASTXMITCOUNT,
	CTL_GETLASTPIPENO,
	CTL_LAST = CTL_GETLASTPIPENO
};

/* We assume that an ioctl can be a set or a get. Not both. */
static struct ctl_el ctls[] = {
	[CTL_SETRADIOFREQ] = {.ioctl = WLIOC_SETRADIOFREQ, .size_i = sizeof(uint32_t)},
	[CTL_GETRADIOFREQ] = {.ioctl = WLIOC_GETRADIOFREQ, .size_o = sizeof(uint32_t)},
	[CTL_SETTXPOWER] = {.ioctl = WLIOC_SETTXPOWER, .size_i = sizeof(int32_t)},
	[CTL_GETTXPOWER] = {.ioctl = WLIOC_GETTXPOWER, .size_o = sizeof(int32_t)},
	[CTL_SETRETRCFG] = {.ioctl = NRF24L01IOC_SETRETRCFG, .size_i = sizeof(nrf24l01_retrcfg_t)},
	[CTL_GETRETRCFG] = {.ioctl = NRF24L01IOC_GETRETRCFG, .size_o = sizeof(nrf24l01_retrcfg_t)},
	[CTL_SETPIPESCFG] = {.ioctl = NRF24L01IOC_SETPIPESCFG, .size_i = sizeof(nrf24l01_pipecfg_t)*NRF24L01_PIPE_COUNT},
	[CTL_GETPIPESCFG] = {.ioctl = NRF24L01IOC_GETPIPESCFG, .size_o = sizeof(nrf24l01_pipecfg_t)*NRF24L01_PIPE_COUNT},
	[CTL_SETPIPESENABLED] = {.ioctl = NRF24L01IOC_SETPIPESENABLED, .size_i = sizeof(uint8_t)},
	[CTL_GETPIPESENABLED] = {.ioctl = NRF24L01IOC_GETPIPESENABLED, .size_o = sizeof(uint8_t)},
	[CTL_SETDATARATE] = {.ioctl = NRF24L01IOC_SETDATARATE, .size_i = sizeof(nrf24l01_datarate_t)},
	[CTL_GETDATARATE] = {.ioctl = NRF24L01IOC_GETDATARATE, .size_o = sizeof(nrf24l01_datarate_t)},
	[CTL_SETADDRWIDTH] = {.ioctl = NRF24L01IOC_SETADDRWIDTH, .size_i = sizeof(uint32_t)},
	[CTL_GETADDRWIDTH] = {.ioctl = NRF24L01IOC_GETADDRWIDTH, .size_o = sizeof(uint32_t)},
	[CTL_SETSTATE] = {.ioctl = NRF24L01IOC_SETSTATE, .size_i = sizeof(nrf24l01_state_t)},
	[CTL_GETSTATE] = {.ioctl = NRF24L01IOC_GETSTATE, .size_o = sizeof(nrf24l01_state_t)},
	[CTL_GETLASTXMITCOUNT] = {.ioctl = NRF24L01IOC_GETLASTXMITCOUNT, .size_o = sizeof(uint32_t)},
	[CTL_GETLASTPIPENO] = {.ioctl = NRF24L01IOC_GETLASTPIPENO, .size_o = sizeof(uint32_t)}
};

union ctls_data {
	uint32_t d_u32;
	int32_t d_32;
	nrf24l01_retrcfg_t d_retrcfg;
	nrf24l01_pipecfg_t d_pipecfg[NRF24L01_PIPE_COUNT];
	uint8_t d_u8;
	nrf24l01_datarate_t d_datarate;
	nrf24l01_state_t d_state;
};

union ctls_data cdata;	/* too big for the stack on low memory systems */
static nrf24l01_pipecfg_t *pcfg_ptr[NRF24L01_PIPE_COUNT] = {
	&cdata.d_pipecfg[0],
	&cdata.d_pipecfg[1],
	&cdata.d_pipecfg[2],
	&cdata.d_pipecfg[3],
	&cdata.d_pipecfg[4],
	&cdata.d_pipecfg[5],
};

/*
struct frame {
	unsigned char pipe_id;
	unsigned char addr[5];
	unsigned char *data;
};
*/
static inline void dump_mem(const unsigned char *b, size_t s);
static int decode_ppdata(struct pp_data *d, int dev_fd, int sock);
static inline size_t frame_head(unsigned char *fr_p, size_t d_len);
static size_t encode_frame_rcr(unsigned char *fr_p, uint8_t pt_radio, uint32_t rc);

int pp_init(struct pp_data *d)
{
	if (!d)
		return -EINVAL;
	d->fs = PP_FS_IDLE;
	d->magic_n = 0;

	return 0;
}

static inline void dump_mem(const unsigned char *b, size_t s)
{
	while (s--)
		dprintf("%02x ", *b++);
	dprintf("\n");
}

/* Decode and execute the pp frame received. If requested, it send back
 * a response containing the outcome of the operation.
 *
 * WARNING:
 * To save space we use the same frame_buf to build the response
 * frame.
 */
static int decode_ppdata(struct pp_data *d, int dev_fd, int sock)
{
	size_t i = 0;
	int ret = 0;
	unsigned char pt_radio = 0;
	union ctls_data *ctld = &cdata;

	/* TODO: make a decent check of the frame length / payload */
	if (d->f_size < 7) {
		ret = EINVAL;
		goto rcr_and_ret;
	}

	while (i < d->f_size)
		dprintf("%02X", d->frame_buf[i++]);
	dprintf("\n");

	pt_radio = PP_PIPE_RADIO(d->frame_buf[PP_PIPE_ID_OFFSET]);

	if (pt_radio) {
		/* forward frame */
		if (ioctl(dev_fd, NRF24L01IOC_SETTXADDR, (unsigned long)
			  (d->frame_buf+PP_ADDR_OFFSET)) < 0) {
			dprintf("SETTXADDR failed: %d\n" -errno);
			ret = errno;
			goto rcr_and_ret;
		}
		if (write(dev_fd, &d->frame_buf[PP_DATA_OFFSET],
			  d->f_size - PP_DATA_OFFSET) < 0) {
			dprintf("dev write failed: %d\n", -errno);
			ret = errno;
			goto rcr_and_ret;
		}
	} else {
		uint8_t ioc_idx;
		/* control frame */
		/* ignore addr field; not needed */
		if (d->frame_buf[PP_CTLD_IOC_OFFSET] > CTL_LAST) {
			dprintf("Unknown ctl: %u\n",
				d->frame_buf[PP_CTLD_IOC_OFFSET]);
			ret = EINVAL;
			goto rcr_and_ret;
		}
		ioc_idx = d->frame_buf[PP_CTLD_IOC_OFFSET];

		if (ctls[ioc_idx].size_o) {
			unsigned char *pp_p;
			/* We received an ioctl of type "get" */
			if (ioc_idx == CTL_GETPIPESCFG) {
				/* This ioctl wants an array of pointer to
				 * structs. Change on the fly the data
				 * pointer and restore later. */
				ctld = (union ctls_data *)pcfg_ptr;
			}
			if (ioctl(dev_fd, ctls[ioc_idx].ioctl,
				  (unsigned long)((uint8_t *)ctld)) < 0) {
				ret = errno;
				dprintf("ioctl fail: %d\n", -ret);
				goto rcr_and_ret;
			}
			ctld = &cdata;
			/* Here we build the entire frame, thus we need to
			 * consider the frame header. */
			/* !! Be careful: here we overwrite the frame_buf !! */
			pp_p = d->frame_buf+FRAME_PP_OFFSET;
			pp_p[PP_PIPE_ID_OFFSET] =
				PP_RCR_MASK |
				PP_PIPE_TYPE_MASK;
			/* clear addr */
			memset(&pp_p[PP_ADDR_OFFSET], 0, PP_DATA_OFFSET-PP_ADDR_OFFSET);
			pp_p[PP_CTLD_IOC_OFFSET] = ioc_idx;
			memcpy(&pp_p[PP_CTLD_DATA_OFFSET], ctld, ctls[ioc_idx].size_o);
			frame_head(d->frame_buf, PP_CTLD_DATA_OFFSET+ctls[ioc_idx].size_o);
			if (write(sock, d->frame_buf, FRAME_PP_OFFSET +
				  PP_CTLD_DATA_OFFSET + ctls[ioc_idx].size_o) < 0)
				dprintf("failed to send rcr: %d\n", -errno);

			dump_mem((unsigned char *)ctld, ctls[ioc_idx].size_o);
			goto exit_ret;

		}

		if (ctls[ioc_idx].size_i) {
			if ((d->f_size - PP_CTLD_DATA_OFFSET) !=
			    ctls[ioc_idx].size_i) {
				dprintf("Expected %d B, received %d B\n",
					ctls[ioc_idx].size_i,
					d->f_size - PP_CTLD_DATA_OFFSET);
				ret = EINVAL;
				goto rcr_and_ret;
			}
			/* TODO: Check the endianess!
			 * Also, to save some bytes you could avoid copying data
			 * and pass directly the buffer pointer to the ioctl...
			 * However you are tied to the endianess of the system which
			 * must be followed on the protocol bytes order. */
			memcpy(ctld, &d->frame_buf[PP_CTLD_DATA_OFFSET],
			       ctls[ioc_idx].size_i);

			dump_mem((unsigned char *)ctld, ctls[ioc_idx].size_i);

			if (ioc_idx == CTL_SETPIPESCFG) {
				/* see above */
				ctld = (union ctls_data *)pcfg_ptr;
			}

			if (ioctl(dev_fd, ctls[ioc_idx].ioctl,
				  (unsigned long)((uint8_t *)ctld)) < 0) {
				ret = errno;
				dprintf("ioctl fail: %d\n", -ret);
				goto rcr_and_ret;
			}
			ctld = &cdata;

			goto exit_ret;
		}
	}

rcr_and_ret:
	if (PP_RCR(d->frame_buf[PP_PIPE_ID_OFFSET])) {
		i = encode_frame_rcr(d->frame_buf, pt_radio, ret);
		if (write(sock, d->frame_buf, i) < 0)
			dprintf("failed to send rcr: %d\n", -errno);
	}

exit_ret:
	return -ret;
}

static inline size_t frame_head(unsigned char *fr_p, size_t d_len)
{
	*(fr_p++) = MAGIC[0];
	*(fr_p++) = MAGIC[1];
	*(fr_p++) = MAGIC[2];
	*(fr_p++) = MAGIC[3];
	*(fr_p++) = (d_len >> 8);
	*(fr_p++) = d_len & 0xff;
	return 6;
}

/* Encode return code response from either an ioctl or a write.
 *
 * WARNING: it does not check buffer size! It must be large enough to hold
 * an rcr frame! */
static size_t encode_frame_rcr(unsigned char *fr_p, uint8_t pt_radio, uint32_t rc)
{
	unsigned char *pp_p = fr_p+FRAME_PP_OFFSET;

	memset(pp_p, 0, PP_DATA_OFFSET); /* clear the pp header */
	*pp_p |= PP_RCR_MASK;
	*pp_p |= pt_radio ? 0 : (PP_PIPE_TYPE_MASK);
	pp_p += PP_DATA_OFFSET;
	*(pp_p++) = (rc >> 24);
	*(pp_p++) = (rc >> 16);
	*(pp_p++) = (rc >> 8);
	*(pp_p++) = rc;
	frame_head(fr_p, pp_p-(fr_p+FRAME_PP_OFFSET));
	return pp_p-fr_p;
}

int pp_fetch(struct pp_data *d, const unsigned char *buf_in,
	     size_t len_in, int dev_fd, int sock)
{
	int f_complete = 0;

	if (!d || !buf_in)
		return -EINVAL;

	while (len_in > 0) {
		switch(d->fs) {
		case PP_FS_IDLE:
			if (*buf_in == MAGIC[0]) {
				d->fs = PP_FS_MAGIC;
				d->magic_n = 1;
			}
			break;

		case PP_FS_MAGIC:
			if (d->magic_n == 4) {
				d->f_size = (*buf_in) << 8;
				d->fs = PP_FS_SIZE;
				break;
			}

			if (*buf_in == MAGIC[d->magic_n++]) {
				if (d->magic_n == 4)
					dprintf("Magic recv\n");
				break;
			}
			else
				d->fs = PP_FS_IDLE;
			break;
		case PP_FS_SIZE:
			d->f_size |= (*buf_in);
			d->f_left = d->f_size;
			if (d->f_size > FRAME_SIZE_MAX) {
				d->fs = PP_FS_IDLE; /* frame discarded */
			} else {
				dprintf("Size: %u\n", d->f_size);
				d->fs = PP_FS_DATA;
			}
			break;
		case PP_FS_DATA:
			d->frame_buf[d->f_size-d->f_left] = *buf_in;
			if (--d->f_left == 0) {
				f_complete++;
				d->fs = PP_FS_IDLE;
				decode_ppdata(d, dev_fd, sock);
				/* TODO: in base a cosa mi ritorna e se richiesto,
				 devi rispondere subito al client con encode_frame_rcr.
				(forse conviene fargli rispondere direttamente dalla
				decode_ppdata?)*/
			}
			break;
		}
		buf_in++;
		len_in--;
	}

	return f_complete;
}

/* Read data from device and encode a frame to send.
 *
 * WARNING:
 * To save space, it is used the receiving buffer to build the response
 * frame
 */
int pp_encode(unsigned char *buf, size_t buf_len, int dev_fd)
{
	size_t n_in = 0;
	unsigned char *data, *head;

	if (!buf)
		return -EINVAL;

	if (buf_len < FRAME_PP_OFFSET+PP_DATA_OFFSET) {
		dprintf("Bigger buffer required\n");
		return -EINVAL;
	}
	data = buf+FRAME_PP_OFFSET+PP_DATA_OFFSET;
	head = buf;

	if ((n_in = read(dev_fd, data, buf_len-(FRAME_PP_OFFSET+PP_DATA_OFFSET))) == 0) {
		dprintf("nothing to read !?!?!\n");
		return -EIO;
	} else {
		uint32_t pipeno;
		frame_head(head, n_in + PP_DATA_OFFSET);
		if (ioctl(dev_fd, NRF24L01IOC_GETLASTPIPENO,
			  (unsigned long)((uint32_t *)&pipeno)) < 0)
			return -errno;
		head += FRAME_PP_OFFSET;
		*(head++) = pipeno & PP_PIPE_SRC_MASK;
		memset(head, 0, PP_DATA_OFFSET-PP_ADDR_OFFSET);
	}

	return n_in + FRAME_PP_OFFSET+PP_DATA_OFFSET;
}
